Passo a passo projeto.

**Obs:** Caso não tenha o docker instalado, deverá fazer a instalação e iniciar o mesmo.

1- baixar o repositório para o seu computador local:
https://gitlab.com/bruno_rocha5/calculo-de-tintas.git

2- Abrir o prompt de comando (Windows + R, digitar cmd e teclar enter)e navegar até sua pasta onde salvou os arquivos: Ex: C:\Users\bruno\Desktop\teste_DR

3- Após localizar a pasta do seu projeto no prompt deve digitar esse comando para criar a imagem do docker:
`docker build --pull --rm -f "Dockerfile" -t testedr:latest "."`

4- Deve digitar esse comando para criar o container com o seguinte comando:
 `docker compose -f "docker-compose.yml" up -d --build`

5- Pode entrar no seu navegador de preferência e digitar localhost, deve aparecer a tela do Projeto Cálculo de tintas.
