<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cálculo para pintar paredes</title>
    <link rel="stylesheet" href="style.css">
    <script>
        function mascara(i) {

            var v = i.value;  
            
            if(isNaN(v[v.length-1])){ // impede entrar outro caractere que não seja número
                i.value = v.substring(0, v.length-1);
                return;
            }  

            i.setAttribute("maxlength", "5");

            if (v.length == 1){ 
                i.value += ".";
            }

            if (v.length == 5){ 
                var ireplace = i.value.replace('.',''); //limpa os pontos
                var add = ".";
                var position = 2;
                var output = [ireplace.slice(0, position), add, ireplace.slice(position)].join('');
                i.value = output;
            }
        }
    </script>
</head>
<body>
    <h1>Cálculo para Pintar Paredes</h1>

    <form action="save.php" method="POST">
        <?php
            for($i = 1; $i <= 4; $i++){   
        ?>
        <div class='box'>
            <h3>Parede <?php echo $i ?></h3>
            <label>Altura:</label>
            <input type="text" name="altura[]" oninput="mascara(this)">
            <label>Largura:</label>
            <input type="text" name="largura[]" oninput="mascara(this)"> 
            <label>Qtde Porta(s):</label>
            <input type="number" name="qtporta[]" min="0"> 
            <label>Qtde Janela(s):</label>
            <input type="number" name="qtjanela[]" min="0">
        </div>
        <?php } ?>
        </br>

        <input type="submit" class='button' name="Calcular" value="Calcular">
    
    </form>
    
</body>
</html>

