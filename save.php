<!DOCTYPE html>
<html lang="pt-BR">
<script>
    function voltar(){
        window.history.back();
    }
</script>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cálculo pintar paredes</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <input type="button" class='button' onclick="voltar()" value="Voltar"><br>
</body>

</html>

<?php

$dados = $_POST;

$somaAltura = array_sum($dados['altura']);
$somaLargura = array_sum($dados['largura']);
$somaPorta = array_sum($dados['qtporta']);
$somaJanela = array_sum($dados['qtjanela']);
$tinta = 0;

if($somaAltura < 1 || $somaLargura < 1){
    die('Preenche os campos altura e largura corretamente.');
}

$metroQuadradoParede = metroQuadradoParede($dados);
if(is_string($metroQuadradoParede)){
    die($metroQuadradoParede);    
}

//Validando parede
function metroQuadradoParede($dados){

    $altura = $dados['altura'];
    $largura = $dados['largura'];
    
    
    for ($i=0; $i < count($altura); $i++) { 
        
        if($altura[$i] > 0 && $largura[$i] > 0){

            $metroQuadradoParede = $altura[$i] * $largura[$i];

            if($metroQuadradoParede < 1 ){
                return 'A metragem da parede tem que ser maior que 1 M²';
            }
            if ($metroQuadradoParede > 50){
                return 'A metragem da parede tem que ser menor que 50 M²';
            }
        }
    }

return;
}

$metroQuadrado = metroQuadrado($somaAltura, $somaLargura);
if(is_string($metroQuadrado)){
    die($metroQuadrado);    
} else {
    $tinta = $metroQuadrado / 5;
}

# Se tiver portas ou janelas
if($somaPorta >= 1 || $somaJanela >= 1){
    echo $validaPortaJanela = validaPortaJanela($metroQuadrado, $somaAltura);
    
    if(is_null($validaPortaJanela)){
        echo respostaOK($metroQuadrado, $tinta);
    }
} else {
    echo respostaOK($metroQuadrado, $tinta);
}

function metroQuadrado($altura, $largura){
    $metroQuadrado = $altura * $largura;
    $dados = $_POST;
    $tamanhoJanela = (2.00 * 1.20);
    $tamanhoPorta = (0.80 * 1.90);

    $porta1 = $dados['qtporta'][0];
    $porta2 = $dados['qtporta'][1];
    $porta3 = $dados['qtporta'][2];
    $porta4 = $dados['qtporta'][3];

    if($porta1 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoPorta * $porta1);
    }
    if($porta2 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoPorta * $porta2);
    }
    if($porta3 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoPorta * $porta3);
    }
    if($porta4 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoPorta * $porta4);
    }

    $janela1 = $dados['qtjanela'][0];
    $janela2 = $dados['qtjanela'][1];
    $janela3 = $dados['qtjanela'][2];
    $janela4 = $dados['qtjanela'][3];

    if($janela1 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoJanela * $janela1);
    }
    if($janela2 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoJanela * $janela2);
    }
    if($janela3 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoJanela * $janela3);
    }
    if($janela4 >= 1){
        $metroQuadrado = $metroQuadrado - ($tamanhoJanela * $janela4);
    }

    return $metroQuadrado;
    
}

function validaPortaJanela($metroQuadrado, $altura){

    $maxArea = $metroQuadrado / 2;
    $tamanhoJanela = (2.00 * 1.20);
    $tamanhoPorta = (0.80 * 1.90);
    $areaportajanela = $tamanhoJanela + $tamanhoPorta;
    $dados = $_POST;
    $altura = $dados['altura'];
    $tinta = $metroQuadrado / 5;

    //VALIDAÇOES ALTURA PORTAS
    if($dados['qtporta'][0] >= 1 ){
        if($altura[0] < 2.20){
            return 'A parede 1 deve ser 30 centímetros maior que a altura da porta (1.90cm)';
        }

    }
    if($dados['qtporta'][1] >= 1 ){
        if($altura[1] < 2.20){
            return 'A parede 2 deve ser 30 centímetros maior que a altura da porta (1.90cm)';
        }

    }
    if($dados['qtporta'][2] >= 1 ){
        if($altura[2] < 2.20){
            return 'A parede 3 deve ser 30 centímetros maior que a altura da porta (1.90cm)';
        }

    }
    if($dados['qtporta'][3] >= 1 ){
        if($altura[3] < 2.20){
            return 'A parede 4 deve ser 30 centímetros maior que a altura da porta (1.90cm)';
        }

    }

    //VALIDAÇÃO AREA TOTAL PAREDES
    if($areaportajanela > $maxArea){
        return 'O total de área das portas e janelas deve ser no máximo 50% da área de parede';
    }

    return;
}

function calculaLatasTinta($litros){
    $latasDisponiveis = array(18, 3.6, 2.5, 0.5);
    $latas = array();

    foreach($latasDisponiveis as $lata)
    {
        while($lata <= $litros)
        {       
            array_push($latas, $lata);
            $litros -= $lata;
        }       
        if($litros < 0.5 && $litros > 0){
            array_push($latas, 0.5);
            $litros -= $lata;
        }
    }
    return $latas;
}

function respostaOK($metroQuadrado , $tinta){

    $calculaLatasTinta = calculaLatasTinta($tinta);

    $mensagemLatas = "Você vai precisar de: ";
    foreach ($calculaLatasTinta as $key => $value) {
        $mensagemLatas .= "<li>Lata de ".$value."</li>";
    }
        
    return '<p>Para pintar a area da parede '.$metroQuadrado.' M² é necessário '.$tinta.' litros de tinta</p><p>'.$mensagemLatas.'</p>';
}





